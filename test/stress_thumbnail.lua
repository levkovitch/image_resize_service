-- stress_thumbnail.lua
--
local wrk = _G.wrk

local endpoint = "/image/thumbnail"

local width = 240
local height = 368

local image_file_name = "image.png"
local image_mime = "image/png"
local image_body

function _G.setup(thread)
  if image_body == nil then
    image_body = io.open(image_file_name, "r"):read("*a")
  end

  thread:set("mime", image_mime)
  thread:set("body", image_body)
end

function _G.request()
  local mime = wrk.thread:get("mime")
  local body = wrk.thread:get("body")
  return wrk.format("POST", string.format("%s?width=%d&height=%d", endpoint,
                                          width, height),
                    {["Content-Type"] = mime}, body)
end

function _G.response(status, _, body)
  if status ~= 200 then
    print(string.format("invalid response: %d %s", status, body or ""))
  end
end
