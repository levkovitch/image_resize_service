// image_resize_service.cpp

#include "image_service.hpp"
#include "asserts.hpp"
#include "simple_logs/logs.hpp"
#include "stats_service.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <regex>
#include <restpp/http.hpp>


#define WIDTH_ARG  "width"
#define HEIGHT_ARG "height"

#define MIN_WIDTH  12
#define MIN_HEIGHT 12
#define MAX_WIDTH  2048
#define MAX_HEIGHT 2048

#define MIME_JPEG      "image/jpeg"
#define MIME_JPG       "image/jpg"
#define MIME_PNG       "image/png"
#define MIME_ANY       "*/*"
#define MIME_ANY_IMAGE "image/*"
#define MIME_JSON      "application/json"


namespace http = restpp::http;


class service_exception final : public std::exception {
public:
  service_exception(std::string_view what)
      : what_{what} {
  }

  const char *what() const noexcept override {
    return what_.c_str();
  }

private:
  std::string what_;
};

cv::Mat     deserialize_image(std::string_view body);
std::string serialize_image(cv::Mat img, std::string mime);

cv::Mat create_thumbnail(const cv::Mat &image, cv::Size thumbnail_size);
cv::Mat transparency_to_white(const cv::Mat &image);


template <typename Method>
image_service::callback bind_handler(Method method) {
  return std::bind(method,
                   std::placeholders::_1,
                   std::placeholders::_2,
                   std::placeholders::_3);
}

image_service::image_service() {
  using namespace http::literals;

  handlers_map_[http::verb::post].emplace_back(
      "/thumbnail"_path,
      bind_handler(&image_service::thumbnail));

  handlers_map_[http::verb::post].emplace_back(
      "/size",
      bind_handler(&image_service::size));
}

void image_service::handle(http::request &req, http::response &res) {
  using namespace http::literals;

  stats_service::increment(req);


  http::verb verb = req.method();
  if (handlers_map_.count(verb) == 0) {
    LOG_WARNING("method: %1% - not allowed", req.method_string());

    res.result(http::status::method_not_allowed);
    return;
  }


  http::url::path path{http::url::get_path(req.relative())};

  image_service::callback current_callback;
  for (auto [name, foo] : handlers_map_[verb]) {
    if (name == path) {
      current_callback = foo;
      break;
    }
  }

  if (current_callback == nullptr) {
    LOG_WARNING("not found: %1%", path);

    res.result(http::status::not_found);
    return;
  }


  // handling
  try {
    current_callback(this, req, res);
  } catch (service_exception &e) {
    LOG_ERROR("catch service exception: %1%", e.what());

    res.result(http::status::expectation_failed);
    res.body() = e.what();
  } catch (std::exception &) {
    throw;
  }
}

void image_service::thumbnail(http::request &req, http::response &res) const {
  std::string_view query_str  = http::url::get_query(req.relative());
  http::url::args  query_args = http::url::query::split(query_str);


  // check params
  std::string req_len_str{req[http::header::content_length]};
  size_t      req_len = 0;
  try {
    req_len = std::stoi(req_len_str);
  } catch (std::exception &e) {
    // do nothing, becuase req_len == 0 - handle it bellow
  }

  if (req_len == 0) {
    LOG_WARNING("empty content-length");

    res.result(http::status::precondition_failed);
    res.body() = "empty content-length";
    return;
  }

  std::string input_mime_type{req[http::header::content_type]};
  if (input_mime_type != MIME_JPEG && input_mime_type != MIME_JPG &&
      input_mime_type != MIME_PNG) {
    LOG_WARNING("unsupported content-type: %1%", input_mime_type);

    res.result(http::status::unsupported_media_type);
    res.body() = "unsupported content-type: " + input_mime_type;
    return;
  }


  std::string output_mime_type{req[http::header::accept]};
  if (output_mime_type.empty() || output_mime_type == MIME_ANY ||
      output_mime_type == MIME_ANY_IMAGE) {
    output_mime_type = input_mime_type;
  } else if ((output_mime_type == MIME_JPEG || output_mime_type == MIME_JPG ||
              output_mime_type == MIME_PNG) == false) {
    LOG_WARNING("unsupported accept type: %1%", output_mime_type);

    res.result(http::status::not_acceptable);
    res.body() = "unsupported accept type: " + output_mime_type;
    return;
  }


  if (query_args.count(WIDTH_ARG) == 0 || query_args.count(HEIGHT_ARG) == 0) {
    LOG_WARNING("not found needed args: %1%/%2%", WIDTH_ARG, HEIGHT_ARG);

    res.result(http::status::precondition_failed);
    res.body() = "arg not found: " WIDTH_ARG "/" HEIGHT_ARG;
    return;
  }

  int width  = 0;
  int height = 0;
  try {
    width  = std::stoi(query_args[WIDTH_ARG]);
    height = std::stoi(query_args[HEIGHT_ARG]);
  } catch (std::exception &e) {
    LOG_WARNING("invalid args");

    res.result(http::status::precondition_failed);
    res.body() = "invalid arg: " WIDTH_ARG "/" HEIGHT_ARG;
    return;
  }

  if (width < MIN_WIDTH || height < MIN_HEIGHT || width > MAX_WIDTH ||
      height > MAX_HEIGHT) {
    LOG_WARNING("invalid arg values");

    res.result(http::status::precondition_failed);
    res.body() = "invalid value of arg: " WIDTH_ARG "/" HEIGHT_ARG;
    return;
  }


  // read body and handle image
  req.read_body();


  cv::Mat image     = deserialize_image(req.body());
  cv::Mat thumbnail = create_thumbnail(image, cv::Size{width, height});

  res.set(http::header::content_type, output_mime_type);
  res.body() = serialize_image(thumbnail, output_mime_type);


  LOG_DEBUG("return thumbnail: %1% %2%x%3%", output_mime_type, width, height);
}

void image_service::size(http::request &req, http::response &res) const {
  // check params
  std::string req_len_str{req[http::header::content_length]};
  size_t      req_len = 0;
  try {
    req_len = std::stoi(req_len_str);
  } catch (std::exception &e) {
    // do nothing, becuase req_len == 0 - handle it bellow
  }

  if (req_len == 0) {
    LOG_WARNING("empty content-length");

    res.result(http::status::precondition_failed);
    res.body() = "empty content-length";
    return;
  }

  std::string input_mime_type{req[http::header::content_type]};
  if (input_mime_type != MIME_JPEG && input_mime_type != MIME_JPG &&
      input_mime_type != MIME_PNG) {
    LOG_WARNING("unsupported content-type: %1%", input_mime_type);

    res.result(http::status::unsupported_media_type);
    res.body() = "unsupported content-type: " + input_mime_type;
    return;
  }


  // read body and get size of image
  req.read_body();

  cv::Mat  image = deserialize_image(req.body());
  cv::Size size  = image.size();


  // print response
  boost::format fmt(R"({"width": %1%, "height": %2%})");
  fmt % size.width % size.height;

  res.set(http::header::content_type, MIME_JSON);
  res.body() = fmt.str();


  LOG_DEBUG("return image size: %1%x%2%", size.width, size.height);
}


cv::Mat transparency_to_white(const cv::Mat &image) {
  ASSERT(image.type() == CV_8UC4, "image has no transparency");

  std::vector<cv::Mat> separate_channels;
  cv::split(image, separate_channels);

  // create mask for BGR channels from alpha channel
  cv::Mat mask;
  separate_channels[3].copyTo(mask);

  cv::threshold(mask, mask, 0, 255, cv::THRESH_BINARY);
  cv::bitwise_not(mask, mask);

  // change
  cv::bitwise_or(separate_channels[0], mask, separate_channels[0]);
  cv::bitwise_or(separate_channels[1], mask, separate_channels[1]);
  cv::bitwise_or(separate_channels[2], mask, separate_channels[2]);

  // and unite
  cv::Mat retval;
  cv::merge(separate_channels, retval);
  return retval;
}

cv::Mat deserialize_image(std::string_view body) {
  const cv::Mat buf(1, body.size(), CV_8UC1, const_cast<char *>(body.data()));

  cv::Mat retval = cv::imdecode(buf, cv::IMREAD_UNCHANGED);
  if (retval.empty()) {
    LOG_THROW(service_exception, "invalid image");
  }

  return retval;
}

std::string serialize_image(cv::Mat img, std::string mime) {
  const std::regex mime_reg{R"(image/(\w+))"};

  std::string type;
  if (std::smatch match;
      std::regex_match(mime, match, mime_reg) && match.size() == 2) {
    type = match[1];
  } else {
    LOG_THROW(service_exception, "invalid mime");
  }


  // XXX here is a problem with serializing to jpg/jpeg from 4-channel image,
  // because 4-th channel just ignores, but its wrong. Every pixel with alpha
  // channel == 0 must be a white
  if ((mime == MIME_JPG || mime == MIME_JPEG) && img.channels() == 4) {
    img = transparency_to_white(img);
  }


  std::vector<uchar> serialized;
  bool               ok = cv::imencode("." + type, img, serialized);
  if (ok == false) {
    LOG_THROW(service_exception, "can't encode image");
  }


  std::string retval;
  std::copy(serialized.begin(), serialized.end(), std::back_inserter(retval));
  return retval;
}

cv::Mat create_thumbnail(const cv::Mat &image, cv::Size thumbnail_size) {
  cv::Size current_size = image.size();

  double scale_x = current_size.width / double(thumbnail_size.width);
  double scale_y = current_size.height / double(thumbnail_size.height);
  double scale   = std::min(scale_x, scale_y);

  int x = (scale == scale_x)
              ? 0
              : (current_size.width - thumbnail_size.width * scale) / 2;
  int y = (scale == scale_y)
              ? 0
              : (current_size.height - thumbnail_size.height * scale) / 2;

  cv::Rect rect_thumbnail(x,
                          y,
                          thumbnail_size.width * scale,
                          thumbnail_size.height * scale);

  cv::Mat output;
  cv::resize(image(rect_thumbnail), output, {}, 1. / scale, 1. / scale);

  return output;
}
