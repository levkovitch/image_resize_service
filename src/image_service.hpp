// image_service.hpp

#pragma once

#include <list>
#include <restpp/http/url.hpp>
#include <restpp/http/verb.hpp>
#include <restpp/service.hpp>


class image_service final : public restpp::service {
public:
  using callback       = std::function<void(image_service *         service,
                                      restpp::http::request & req,
                                      restpp::http::response &res)>;
  using named_callback = std::pair<restpp::http::url::path, callback>;
  using callback_list  = std::list<named_callback>;
  using verb_callbacks_map =
      std::unordered_map<restpp::http::verb, callback_list>;


  image_service();

  /**\brief image handling
   * has next endpoints
   *  - /thumbnail  - provide access to thumbnail method
   *  - /size       - provide access to size method
   */
  void handle(restpp::http::request &req, restpp::http::response &res) override;


  /**\brief create thumbnail from input image
   * require two arguments:
   *  - width   as integer
   *  - height  as integer
   */
  void thumbnail(restpp::http::request &req, restpp::http::response &res) const;

  /**\brief print size of image to response as json
   */
  void size(restpp::http::request &req, restpp::http::response &res) const;

private:
  verb_callbacks_map handlers_map_;
};


class image_service_factory final : public restpp::service_factory {
public:
  restpp::service_ptr make_service() override {
    return std::make_shared<image_service>();
  }
};
